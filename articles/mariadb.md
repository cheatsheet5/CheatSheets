# MARIA DB

## Install
### Windows from zip:
1. Download zip (check signature)
1. Extract into final folder (ex : C:\Program File\MariaDb\version)
1. Add bin folder to environnement variable (user or system)
1. Run following comman in install folder

        bin\mysql_install_db.exe

## Run
### Windows
- cd into install folder
- run commande:

        bin\mysqld.exe --console

## Connect to db
REQUIREMENT : MAriaDb must be running

    mysql -u user -p

Change root password on first connection:

    ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyN3wP4ssw0rd';
    flush privileges;
    exit;

Create a new user:

    CREATE USER user@localhost IDENTIFIED BY 'newPassword';

Grant privilege to user:

    GRANT ALL ON databasename.* TO toto@localhost;

Create a database:

    CREATE DATABASE databasename;

Use a DB:

    USE databasename;

